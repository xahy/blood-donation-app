//
//  File.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/17/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import Foundation
import SwiftyJSON
import TRON

struct Donor: JSONDecodable{
    let national_id: String
    let city: String
    let name: String
    let email: String
    let phone_number: String
    let auth_token: String
    let blood_group: String
    
    init(json: JSON) {
        national_id = json["national_id"].stringValue
        city = json["city"].stringValue
        name = json["name"].stringValue
        email = json["email"].stringValue
        phone_number = json["phone_number"].stringValue
        auth_token = json["auth_token"].stringValue
        blood_group = json["blood_group"]["name"].stringValue
    }
    
}


