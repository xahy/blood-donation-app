//
//  BloodRequestDonor.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/18/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import Foundation
import SwiftyJSON


struct BloodRequestDonor {
    let status: String
    let donorRequest: DonationRequest
    let created_at: String
    let donated_at: String
    
    init(json:JSON) {
        self.status = json["status"].stringValue
        self.created_at = json["created_at"].stringValue
        self.donorRequest = DonationRequest(json: json["blood_request"])
        self.donated_at = json["donated_at"].stringValue
    }
    
    
}
