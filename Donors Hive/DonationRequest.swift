//
//  DonationRequest.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 11/7/16.
//  Copyright © 2016 Ismail Zahee. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DonationRequest {
    let id: String
    let location: String
    let bloodgroup: String
    let description: String
    let blood_group_id: String
    let hospital: Hospital
    
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.blood_group_id = json["blood_group_id"].stringValue
        self.description = json["description"].stringValue
        let hospitalObject = json["hospital"]
        
        
        self.hospital = Hospital(json: hospitalObject)
        self.location = hospitalObject["address"].stringValue
        
        let bloodGroupObject = json["blood_group"]
        self.bloodgroup = bloodGroupObject["name"].stringValue
        
        
        
        
    }

}
