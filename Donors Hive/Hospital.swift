//
//  Hospital.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 11/7/16.
//  Copyright © 2016 Ismail Zahee. All rights reserved.
//

import Foundation
import MapKit
import SwiftyJSON

class Hospital: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let discipline: String =  "Sculpture"
    var logo_url:String

    init(json: JSON) {
        
        self.locationName = json["address"].stringValue
        self.title = json["name"].stringValue
        let lat = json["lat"].doubleValue
        let long = json["long"].doubleValue
        self.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.logo_url = json["logo"].stringValue
    }
    
    var subtitle: String? {
        return locationName
    }
}
