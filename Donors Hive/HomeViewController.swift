//
//  HomeViewController.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 11/7/16.
//  Copyright © 2016 Ismail Zahee. All rights reserved.
//

import UIKit
import TRON
import SwiftyJSON

class HomeViewController: UITableViewController {
    
    var donationRequests = [DonationRequest]()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        setUpNavigationBar()
        
        
        setUpLocationService()
        refreshControl = UIRefreshControl()
        
        refreshControl?.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        if checkForLogin() {
            
        } else {
            let authViewController = AuthViewController()
            self.present(authViewController,animated: true)
        }
        tableView.register(UINib(nibName: "BloodRequestCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tableView.rowHeight = UITableViewAutomaticDimension

        
        tableView.backgroundColor = .white
        
        
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView(frame: .zero)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fetchBloodRequests()
        
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        fetchBloodRequests()
        
        refreshControl.endRefreshing()
    }
    
    let tron = TRON(baseURL: Constants.baseUrl)
    
    
    class Home: JSONDecodable {
        
        var donationRequests: [DonationRequest]
        
        required init(json:JSON) throws {
            donationRequests = [DonationRequest]()
            if let array = json.array {
                for req in array {
                    let donationRequest: DonationRequest = DonationRequest(json: req)
                    self.donationRequests.append(donationRequest)
                }
            }
            
        }
    }
    
    class JsonError: JSONDecodable {
        
        required init(json:JSON) throws {
            print("JSON ERROR")
        }
    }
    
    fileprivate func fetchBloodRequests() {
        let request: APIRequest<Home, JsonError> = tron.request("/requests")
        if let authkey = UserDefaults.standard.string(forKey: Constants.authKey) {
            if authkey != "" {
                request.headerBuilder = HeaderBuilder(defaultHeaders: ["Authorization": authkey, "Content-type application":"json", "Accept application" : "json"])
                request.perform(withSuccess: { (home) in
                    self.donationRequests = home.donationRequests
                    self.tableView.reloadData()
                    
                }) { (error) in
                    print("Thre was an error with the request",error)
                }
            }
        }
        
    }
    
    fileprivate func setUpLocationService() {
        

        
    }
    
    func setUpNavigationBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 195/255, green: 78/255, blue: 78/255,alpha:1 )
        navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName: UIColor.white]        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        
        
        navigationItem.title = "Latest"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Sign Out", style: .plain, target: self, action: #selector(handleSignOut))
        navigationItem.leftBarButtonItem?.tintColor = .white
    }
    
    func handleSignOut() {
        UserDefaults.standard.set(false, forKey: Constants.isLoggedIn)
        UserDefaults.standard.set("", forKey: Constants.authKey)
        UserDefaults.standard.synchronize()
        
        let authViewController = AuthViewController()
        self.present(authViewController,animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func checkForLogin() -> Bool {
        
        return UserDefaults.standard.bool(forKey: Constants.isLoggedIn)
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return donationRequests.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BloodRequestCell
        cell.configureCell(donationRequest: donationRequests[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
 
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bloodRequestViewController = BloodRequestViewController()
        bloodRequestViewController.donationRequest = donationRequests[indexPath.row]
        self.navigationController!.pushViewController(bloodRequestViewController, animated: true)
    }

}
