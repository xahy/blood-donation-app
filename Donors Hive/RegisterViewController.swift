//
//  RegisterViewController.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 2/19/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import SwiftForms
import SVProgressHUD
import TRON
import SwiftyJSON
import OneSignal

class RegisterViewController: FormViewController {
    
    
    struct Static {
        static let nameTag = "name"
        static let passwordTag = "password"
        static let emailTag = "email"
        static let nationalidTag = "national_id"
        static let phoneTag = "phone_number"
        static let stateTag = "state_id"
        static let cityTag = "city"
        static let bloodgroupTag = "blood_group_id"
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar()
        SVProgressHUD.setDefaultMaskType(.black)
        // Create form instace
        let form = FormDescriptor()
        form.title = "Register"
        
        // Define first section
        let section1 = FormSectionDescriptor(headerTitle: "",footerTitle:"")
        
        var row = FormRowDescriptor(tag: Static.nameTag, type: .text, title: "Full Name")
        row.configuration.cell.required = true
        section1.rows.append(row)
        
        row = FormRowDescriptor(tag: Static.emailTag, type: .email, title: "Email")
        row.configuration.cell.required = true
        section1.rows.append(row)
        
        row = FormRowDescriptor(tag: Static.passwordTag, type: .password, title: "Password")
        row.configuration.cell.required = true
        section1.rows.append(row)
        
        row = FormRowDescriptor(tag: Static.nationalidTag, type: .text, title: "National ID")
        row.configuration.cell.required = true
        section1.rows.append(row)
        
        row = FormRowDescriptor(tag: Static.phoneTag, type: .phone, title: "Contact Number")
        row.configuration.cell.required = true
        section1.rows.append(row)
        
        row = FormRowDescriptor(tag: Static.bloodgroupTag, type: .picker, title: "Blood Group")
        row.configuration.cell.required = true
        row.configuration.cell.showsInputToolbar = true
        row.configuration.selection.options = (["1", "2", "3","4","5","6","7","8"] as [String]) as [AnyObject]
        row.configuration.selection.optionTitleClosure = { value in
            guard let option = value as? String else { return "" }
            switch option {
            case "1":
                return "A+"
            case "2":
                return "B+"
            case "3":
                return "A-"
            case "4":
                return "B-"
            case "5":
                return "AB+"
            case "6":
                return "AB-"
            case "7":
                return "O+"
            case "8":
                return "O-"
            default:
                return ""
            }
        }
        section1.rows.append(row)
        
        
        
        let section2 = FormSectionDescriptor(headerTitle: "",footerTitle:"")
        
        row = FormRowDescriptor(tag: Static.stateTag, type: .picker, title: "State")
        row.configuration.cell.required = true
        row.configuration.cell.showsInputToolbar = true
        row.configuration.selection.options = (["1", "2", "3","4","5","6"] as [String]) as [AnyObject]
        row.configuration.selection.optionTitleClosure = { value in
            guard let option = value as? String else { return "" }
            switch option {
            case "1":
                return "Mahchan Goalhi"
            case "2":
                return "Maafannu"
            case "3":
                return "Henveyru"
            case "4":
                return "Galolhu"
            case "5":
                return "Villigili"
            case "6":
                return "Hulhumale"
            default:
                return ""
            }
        }
        section2.rows.append(row)
        
        
        row = FormRowDescriptor(tag: Static.cityTag, type: .text, title: "City")
        row.configuration.cell.required = true
        section2.rows.append(row)
        
        
        
        
        
        form.sections = [section1,section2]
        
        self.form = form
        
        
    }
    
    func setUpNavigationBar() {
        navigationItem.title = "Register"
        navigationController?.navigationBar.tintColor = .white
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submit(_:)))
    }
    
    class JsonSuccess: JSONDecodable {
        required init(json: JSON) throws {
            
        }
    }

    
    class RegJsonError: JSONDecodable {
        var msg: String
        required init(json: JSON) throws {
            msg = ""
            for (_,values) in json {
                if msg != "" { msg=msg+"\n" }
                msg = msg+values[0].stringValue
            }
        }
    }
    
    let tron = TRON(baseURL: Constants.baseUrl)
    
    func submit(_: UIBarButtonItem!) {
        if UserDefaults.standard.string(forKey: Constants.oneSignalUserId) != nil {
            self.doRegistration()
        } else {
            OneSignal.idsAvailable({(_ userId, _ pushToken) in
                print("UserId:\(userId)")
                UserDefaults.standard.set(userId,forKey:Constants.oneSignalUserId)
                UserDefaults.standard.synchronize()
                self.doRegistration()
            })
        }
        
    }
    
    func doRegistration() {
        if ((self.form.validateForm()) == nil) {
            SVProgressHUD.show()
            var formData = self.form.formValues()
            let onesignaluserid = UserDefaults.standard.string(forKey: Constants.oneSignalUserId)
            formData["one_signal"] = onesignaluserid as AnyObject?
            let request: APIRequest<Donor, RegJsonError> = tron.request("/register")
            request.method = .post
            request.parameters = formData
            request.perform(withSuccess: { (donor) in
                SVProgressHUD.dismiss()
                UserDefaults.standard.set(true, forKey: Constants.isLoggedIn)
                UserDefaults.standard.set(donor.auth_token, forKey: Constants.authKey)
                UserDefaults.standard.synchronize()
                self.navigationController?.dismiss(animated: true, completion: nil)
                
            }) { (regJsonError) in
                
                SVProgressHUD.dismiss()
                self.showAlert(title: "Error", description: (regJsonError.errorModel?.msg)!)
            }
            
            
        } else {
            let alertController = UIAlertController(title: "Missing", message: "Please fill all the fields in the form before submitting", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "OK", style: .cancel) { (action) in
                
            }
            alertController.addAction(cancel)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showAlert(title: String, description: String) {
        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        self.present(alertController, animated: true)
        alertController.addAction(OKAction)
    }
    
}



