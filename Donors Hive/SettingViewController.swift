//
//  SettingViewController.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/16/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import SwiftForms
import SwiftyJSON
import TRON
import SVProgressHUD

class SettingViewController : FormViewController {
    struct Static {
        static let oldpasswordTag = "old_password"
        static let passwordTag = "password"
        static let confirmPasswordTag = "confirm_password"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        
        setUpNavigationBar()
        SVProgressHUD.setDefaultMaskType(.black)
        
        let form = FormDescriptor(title: "Setting Form")
        
        let section1 = FormSectionDescriptor(headerTitle: "Notification", footerTitle: nil)
        
        var row = FormRowDescriptor(tag: "notification_enabled", type: .booleanSwitch, title: "Enable Notification")
        section1.rows.append(row)
        
        
        
        
        let section2 = FormSectionDescriptor(headerTitle: "Change Password", footerTitle: nil)
        
        row = FormRowDescriptor(tag: Static.oldpasswordTag, type: .password,title: "Current Password")
        section2.rows.append(row)
        
        let section3 = FormSectionDescriptor(headerTitle: nil, footerTitle: nil)
        
        row = FormRowDescriptor(tag: Static.passwordTag, type: .password,title: "New Password")
        section3.rows.append(row)
        
        row = FormRowDescriptor(tag: Static.confirmPasswordTag, type: .password,title: "Confirm Password")
        section3.rows.append(row)
        
        form.sections = [section1,section2,section3]
        
        self.form = form
        
        
    }
    
    class JsonSuccess: JSONDecodable {
        required init(json: JSON) throws {
            
        }
    }
    
    
    class JsonError: JSONDecodable {
        var msg: String
        required init(json: JSON) throws {
            msg = json["message"].stringValue
        }
    }
    
    let tron = TRON(baseURL: Constants.baseUrl)
    
    func submitsettings() {
        if ((self.form.validateForm()) == nil) {
            if let authkey = UserDefaults.standard.string(forKey: Constants.authKey) {
                SVProgressHUD.show()
                let formData = self.form.formValues()
                let request: APIRequest<JsonSuccess, JsonError> = tron.request("/changePassword")
                request.headerBuilder = HeaderBuilder(defaultHeaders: ["Authorization": authkey, "Content-type application":"json", "Accept application" : "json"])
                request.method = .post
                request.parameters = formData
                request.perform(withSuccess: { (donor) in
                    SVProgressHUD.dismiss()
                    
                    self.showAlert(title: "Success",description:  "Password has being updated successfully")
                    self.resetForm()
                    self.view.endEditing(true)
                }) { (jsonError) in
                    
                    SVProgressHUD.dismiss()
                    self.showAlert(title: "Error", description: (jsonError.errorModel?.msg)!)
                }
            }
        } else {
            showAlert(title:"Missing",description:  "Some fields are missing")
        }
    }
    
    
    func setUpNavigationBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 195/255, green: 78/255, blue: 78/255,alpha: 1)
        navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationItem.title = "Settings"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submitsettings))
        navigationItem.rightBarButtonItem?.tintColor = .white
    }
    
    func resetForm() {
        self.setValue("" as AnyObject, forTag: Static.oldpasswordTag)
        self.setValue("" as AnyObject, forTag: Static.passwordTag)
        self.setValue("" as AnyObject, forTag: Static.confirmPasswordTag)
    }
    
    func showAlert(title: String, description: String) {
        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        self.present(alertController, animated: true)
        alertController.addAction(OKAction)
    }
}
