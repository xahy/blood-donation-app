//
//  NewsViewController.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 2/19/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import TRON
import SwiftyJSON
import MapKit

class HistoryViewController: UITableViewController {
    
    
    var bloodRequestDonors = [BloodRequestDonor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setUpNavigationBar()
        
        
        tableView.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "Cell")
        tableView.rowHeight = UITableViewAutomaticDimension                
        tableView.backgroundColor = .white
        
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView(frame: .zero)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchBloodRequestsDonor()
    }
    
    
    class JsonSuccess: JSONDecodable {
        var bloodRequestDonors: [BloodRequestDonor]
        required init(json: JSON) throws {
            bloodRequestDonors = [BloodRequestDonor]()
            if let array = json.array {
                for req in array {
                    let bloodRequestDonor: BloodRequestDonor = BloodRequestDonor(json: req)
                    self.bloodRequestDonors.append(bloodRequestDonor)
                }
            }
        }
    }
    
    class JsonError: JSONDecodable {
        required init(json: JSON) throws {
        }
    }
    
    let tron = TRON(baseURL: Constants.baseUrl)
    
    fileprivate func fetchBloodRequestsDonor() {
        
        let request: APIRequest<JsonSuccess, JsonError> = tron.request("/requests/active")
        if let authkey = UserDefaults.standard.string(forKey: Constants.authKey) {
            if authkey != "" {
                request.headerBuilder = HeaderBuilder(defaultHeaders: ["Authorization": authkey, "Content-type application":"json", "Accept application" : "json"])
                request.perform(withSuccess: { (home) in
                    self.bloodRequestDonors = home.bloodRequestDonors
                    self.tableView.reloadData()
                    
                }) { (error) in
                    print("Thre was an error with the request",error)
                }
            }
        }
        
    }
    
    func setUpNavigationBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 195/255, green: 78/255, blue: 78/255,alpha: 1)
        navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationItem.title = "My Requests"
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bloodRequestDonors.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RequestCell
        cell.configureCell(bloodRequestDonor: bloodRequestDonors[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let optionMenu = UIAlertController(title:nil,message: "Choose an option", preferredStyle: .actionSheet)
        let mapAction = UIAlertAction(title: "Open in Map", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let bloodRequestDonor = self.bloodRequestDonors[indexPath.row]
            
            self.openMapForPlace(latitude: bloodRequestDonor.donorRequest.hospital.coordinate.latitude, longitude: bloodRequestDonor.donorRequest.hospital.coordinate.longitude, place_name: bloodRequestDonor.donorRequest.hospital.title!)
            
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            print("Cancelled")
        })
        
        optionMenu.addAction(mapAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
        
        
    }
    
    func openMapForPlace(latitude: CLLocationDegrees, longitude: CLLocationDegrees, place_name: String) {
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = place_name
        mapItem.openInMaps(launchOptions: options)
    }
}
