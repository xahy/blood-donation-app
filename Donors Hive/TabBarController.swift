//
//  TabBarController.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 2/19/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let homeViewController = HomeViewController()
        let homeNavigationController = UINavigationController(rootViewController: homeViewController)
        homeNavigationController.tabBarItem.title = "Recent"
        homeNavigationController.tabBarItem.image = #imageLiteral(resourceName: "Oval")
        
        
        let historyViewController = HistoryViewController()
        let historyNavigationController = UINavigationController(rootViewController: historyViewController)
        historyNavigationController.tabBarItem.title = "My Requests"
        historyNavigationController.tabBarItem.image = #imageLiteral(resourceName: "History")
        
        let profileViewController = ProfileViewController()
        let profileViewNavigationController = UINavigationController(rootViewController: profileViewController)
        profileViewNavigationController.tabBarItem.title = "Profile"
        profileViewNavigationController.tabBarItem.image = #imageLiteral(resourceName: "Profile")
        
        let settingViewController = SettingViewController()
        let settingNavigationController = UINavigationController(rootViewController: settingViewController)
        settingNavigationController.tabBarItem.title = "Setting"
        settingNavigationController.tabBarItem.image = #imageLiteral(resourceName: "Cog")
        
        
        setUpTabBar()
        
        viewControllers = [homeNavigationController,historyNavigationController,profileViewNavigationController,settingNavigationController]
    }
    
    func setUpTabBar() {
        let selectedColor   = UIColor(red: 195/255, green: 78/255, blue: 78/255,alpha: 1)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: selectedColor], for: .selected)
        tabBar.tintColor    = UIColor(red: 195/255, green: 78/255, blue: 78/255,alpha: 1)
        //tabBar.shadowImage = UIImage()
        tabBar.isTranslucent = false
        
        tabBar.backgroundColor = .white
        
    }
    
    
    
}
