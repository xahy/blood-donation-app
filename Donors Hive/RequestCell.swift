//
//  RequestCell.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/18/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import MapleBacon

class RequestCell: UITableViewCell {

    @IBOutlet weak var hospitalImage: UIImageView!
    @IBOutlet weak var hospitalTitle: UILabel!
    @IBOutlet weak var requestStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(bloodRequestDonor: BloodRequestDonor) {
        hospitalTitle.text = bloodRequestDonor.donorRequest.hospital.title
        requestStatus.text = getStatus(status: bloodRequestDonor.status)
        if let image_url = URL(string: bloodRequestDonor.donorRequest.hospital.logo_url) {
            hospitalImage.setImage(withUrl: image_url)
        }
    }
    
    func getStatus(status: String) -> String {
        
        switch status {
        case "accepted":
            return "Please wait for the approval from the hospital"
        case "selected":
            return "You have been selected from the hospital. Please come to the hospital for the donation process"
        default:
            return ""
        }
        
    }
    
}
