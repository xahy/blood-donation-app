//
//  LoginViewController.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 2/14/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import PureLayout
import SVProgressHUD
import TRON
import SwiftyJSON
import OneSignal

class LoginViewController: UIViewController {
    
    let logoImageView: UIImageView = {
        let imageView = UIImageView.newAutoLayout()
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "Logo")
        return imageView
    }()
    
    
    let emailTextFiled: UITextField = {
        let txtField = UITextField.newAutoLayout()
        txtField.placeholder = "Enter Email"
        txtField.layer.borderColor = UIColor.lightGray.cgColor
        txtField.layer.borderWidth = 1.0
        txtField.layer.masksToBounds = true
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 40))
        txtField.leftView = paddingView
        txtField.leftViewMode = UITextFieldViewMode.always
        txtField.layer.cornerRadius = 5
        txtField.keyboardType = UIKeyboardType.emailAddress
        txtField.autocorrectionType = .no
        return txtField
    }()
    
    let passwordTextField: UITextField = {
        let txtField = UITextField.newAutoLayout()
        txtField.placeholder = "Enter password"
        txtField.isSecureTextEntry = true
        txtField.layer.borderColor = UIColor.lightGray.cgColor
        txtField.layer.borderWidth = 1.0
        txtField.layer.masksToBounds = true
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 40))
        txtField.leftView = paddingView
        txtField.leftViewMode = UITextFieldViewMode.always
        txtField.layer.cornerRadius = 5
        return txtField
    }()
    
    let alert = UIAlertController(title: "Alert", message: "Hellow there", preferredStyle: .actionSheet)
    
    let red = UIAlertAction(title: "Turn red", style: .default) { (action) in
        
    }
    
    let loginButton: UIButton = {
        let btn = UIButton.newAutoLayout()
        btn.setTitle("Login", for: .normal)
        btn.backgroundColor = UIColor(red: 96/255, green: 135/255, blue: 193/255, alpha: 1)
        btn.setTitleColor(.white, for: .normal)
        btn.layer.cornerRadius = 20
        return btn;
    }()
    

    
    var didSetupConstraints = false
    
    override func loadView() {
        
        view = UIView()
        view.addSubview(logoImageView)
        view.addSubview(emailTextFiled)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        view.backgroundColor = .white
        view.setNeedsUpdateConstraints() // bootstrap Auto Layout
        
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            logoImageView.autoPin(toTopLayoutGuideOf: self, withInset: 40.0)
            logoImageView.autoAlignAxis(.vertical, toSameAxisOf: self.view)
            
            emailTextFiled.autoPinEdge(.left, to: .left, of: self.view, withOffset: 20)
            emailTextFiled.autoPinEdge(.right, to: .right, of: self.view, withOffset: -20)
            emailTextFiled.autoPinEdge(.top, to: .bottom, of: logoImageView,withOffset: 20)
            emailTextFiled.autoSetDimension(.height, toSize: 50.0)
            
            passwordTextField.autoPinEdge(.left, to: .left, of: self.view, withOffset: 20)
            passwordTextField.autoPinEdge(.right, to: .right, of: self.view, withOffset: -20)
            passwordTextField.autoPinEdge(.top, to: .bottom, of: emailTextFiled,withOffset: 10)
            passwordTextField.autoSetDimension(.height, toSize: 50.0)
            
            loginButton.autoPinEdge(.top, to: .bottom, of: passwordTextField, withOffset: 20)
            loginButton.autoPinEdge(.right, to: .right, of: self.view, withOffset: -20)
            loginButton.autoPinEdge(.left, to: .left, of: self.view, withOffset: 20)
            loginButton.autoSetDimension(.height, toSize: 40.0)
            

        }
        super.updateViewConstraints()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        loginButton.addTarget(self, action: #selector(pressedLoginBtn(sender:)), for: .touchUpInside)
        
        
    }
    
    let tron = TRON(baseURL: Constants.baseUrl)

    class JsonError:JSONDecodable {
        required init(json: JSON) throws {
            print("Error",json)
        }
    }
    
    
    
    func pressedLoginBtn(sender: UIButton!) {
        SVProgressHUD.setDefaultMaskType(.gradient)

        SVProgressHUD.show()
        if UserDefaults.standard.string(forKey: Constants.oneSignalUserId) != nil {
            dologin()
        } else {
            OneSignal.idsAvailable({(_ userId, _ pushToken) in
                print("UserId:\(userId)")
                UserDefaults.standard.set(userId,forKey:Constants.oneSignalUserId)
                UserDefaults.standard.synchronize()
                self.dologin()
            })
        }
    }
    
    
    func dologin() {
        if let email = emailTextFiled.text, let password = passwordTextField.text {            
            if email.isEmpty || password.isEmpty {
                SVProgressHUD.dismiss()
                showAlert(title: "Missing", description: "Email field or Password field cannot be blank")
            } else {
                let onesignaluserid = UserDefaults.standard.string(forKey: Constants.oneSignalUserId)
                let request: APIRequest<Donor, JsonError> = tron.request("/login")
                request.method = .post
                request.parameters = ["email":email, "password":password,"one_signal":onesignaluserid!]
                
                request.perform(withSuccess: { (donor) in
                    SVProgressHUD.dismiss()
                    UserDefaults.standard.set(true, forKey: Constants.isLoggedIn)
                    UserDefaults.standard.set(donor.auth_token, forKey: Constants.authKey)
                    UserDefaults.standard.synchronize()
                    self.dismiss(animated: true, completion: {
                        
                    })
                    
                }) { (error) in
                    self.showAlert(title: "Error", description: "Please enter a valid email and password")
                    SVProgressHUD.dismiss()
                }
            }
            
        } else {
            SVProgressHUD.dismiss()
            showAlert(title: "Error", description: "Please enter a valid email and password")
        }
    }
    
    func showRegisterScreen() {
        let registerViewController = RegisterViewController()
        self.navigationController!.pushViewController(registerViewController, animated: true)
    }
    
    func showAlert(title: String, description: String) {
        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        self.present(alertController, animated: true)
        alertController.addAction(OKAction)
    }
    
    
    func setUpNavigationBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 195/255, green: 78/255, blue: 78/255,alpha:1 )
        navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.barStyle = .blackTranslucent
        
        
        navigationItem.title = "Login"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .plain, target: self, action: #selector(showRegisterScreen))
        navigationItem.rightBarButtonItem?.tintColor = .white
    }
}
