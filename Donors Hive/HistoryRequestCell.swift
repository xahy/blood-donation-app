//
//  HistoryRequestCell.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/19/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import MapleBacon

class HistoryRequestCell: UITableViewCell {

    @IBOutlet weak var hospitalImage: UIImageView!
    @IBOutlet weak var hospitallbl: UILabel!
    @IBOutlet weak var donateddatelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(bloodRequestDonor: BloodRequestDonor) {
        hospitallbl.text = bloodRequestDonor.donorRequest.hospital.title
        donateddatelbl.text = "Donated on: "+bloodRequestDonor.donated_at
        
        if let image_url = URL(string: bloodRequestDonor.donorRequest.hospital.logo_url) {
            hospitalImage.setImage(withUrl: image_url)
        }
    }
    
}
