//
//  ProfileViewController.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/11/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import PureLayout
import TRON
import SwiftyJSON

class ProfileViewController: UITableViewController {
        
    var profile:Profile?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "profileCell")
        tableView.register(UINib(nibName: "HistoryRequestCell", bundle: nil), forCellReuseIdentifier: "requestCell")
        
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = .white
        
        tableView.estimatedRowHeight = 100
        tableView.tableFooterView = UIView(frame: .zero)
        setUpNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchProfile()
    }
    
    class JsonSuccess: JSONDecodable {
        var profile: Profile
        required init(json: JSON) throws {
            profile = Profile(json: json)
        }
    }
    
    class JsonError: JSONDecodable {
        required init(json: JSON) throws {
        }
    }
    
    let tron = TRON(baseURL: Constants.baseUrl)
    
    func fetchProfile() {
        let request: APIRequest<JsonSuccess, JsonError> = tron.request("/profile")
        if let authkey = UserDefaults.standard.string(forKey: Constants.authKey) {
            if authkey != "" {
                request.headerBuilder = HeaderBuilder(defaultHeaders: ["Authorization": authkey, "Content-type application":"json", "Accept application" : "json"])
                request.perform(withSuccess: { (home) in
                    self.profile = home.profile
                    self.tableView.reloadData()
                    
                }) { (error) in
                    print("Thre was an error with the request",error)
                }
            }
        }
    }
    
    
    
    
    
    func setUpNavigationBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 195/255, green: 78/255, blue: 78/255,alpha: 1)
        navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationItem.title = "Profile"
    }
    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if profile != nil {
            if section == 0 {
                return 1
            }
            if let historycount = profile?.history?.count {
                return historycount
            }
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == 0) {
            let cell = generateProfileCell(indexPath: indexPath)
            return cell
        } else {
            let cell = generateHistoryRequestCell(indexPath: indexPath)
            return cell
        }

        
    }
    
    func generateProfileCell(indexPath: IndexPath) -> ProfileCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileCell
        cell.configure(profile: self.profile!)
        cell.selectionStyle = .none
        return cell
    }
    
    func generateHistoryRequestCell(indexPath: IndexPath) -> HistoryRequestCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "requestCell", for: indexPath) as! HistoryRequestCell
        cell.configure(bloodRequestDonor: (self.profile?.history?[indexPath.row])!)
        cell.selectionStyle = .none
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return "Past Donations"
        }
        return nil
    }
    

}
