//
//  ProfileCell.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/19/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var bloodGrouplbl: UILabel!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var dayslastdonatelbl: UILabel!
    @IBOutlet weak var totaldonatelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(profile: Profile) {
        bloodGrouplbl.text = profile.donor.blood_group
        namelbl.text = profile.donor.name
        if (profile.history != nil) {
           totaldonatelbl.text = profile.history?.count.description
        } else {
            totaldonatelbl.text = "0"
        }
        dayslastdonatelbl.text = profile.daysLastDonated
        
        
    }
    
}
