//
//  Service.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/16/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    static let baseUrl = "http://188.166.230.101/api/v1"
    
    static let isLoggedIn = "isLoggedIn"
    
    static let authKey = "AuthKey"
    
    static let oneSignalUserId = "oneSignalUserId"
    
    static let OneSignalKey = "3944f4dc-e5b3-4367-b1b1-3acaa31d1820"
        
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        print("hide keyboard")
        view.endEditing(true)
    }
}
