//
//  Auth.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/17/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit


class AuthViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = .white
        
        let loginViewController = LoginViewController()
        
        viewControllers = [loginViewController]
        
    }
    
    
    

}
