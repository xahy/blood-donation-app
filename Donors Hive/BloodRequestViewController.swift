//
//  BloodRequestViewController.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 2/20/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import UIKit
import PureLayout
import MapKit
import TRON
import SVProgressHUD
import SwiftyJSON

class BloodRequestViewController: UIViewController {
    
    
    
    let regionRadius: CLLocationDistance = 500
    
    let mapView: MKMapView = {
        let map = MKMapView()
        map.mapType = MKMapType.standard
        map.isZoomEnabled = true
        map.isScrollEnabled = true
        return map
    }()
    
    let bloodGroupLabel: UILabel = {
        let label = UILabel.newAutoLayout()
        
        label.backgroundColor = UIColor(red: 195/255, green: 78/255, blue: 78/255, alpha: 1)
        label.numberOfLines = 1
        label.textColor = .white
        label.layer.cornerRadius = 50.0
        label.clipsToBounds = true
        label.textAlignment = .center
        label.font = UIFont(name: label.font.fontName, size: 30)
        return label
    }()
    
    let hostpitalNameLabel: UILabel = {
        let label = UILabel.newAutoLayout()
        label.numberOfLines = 0
        label.text = NSLocalizedString("Medical center of Subang J", comment: "")
        label.font = UIFont(name: label.font.fontName, size: 17)
        return label
    }()
    
    let accpetbutton: UIButton = {
        let btn = UIButton.newAutoLayout()
        btn.setTitle("Accept Request", for: .normal)
        btn.setTitleColor(.white, for: .normal)
        btn.backgroundColor = UIColor(red: 34/255, green: 201/255, blue: 17/255, alpha: 1) //green
        btn.layer.cornerRadius = 5.0
        btn.clipsToBounds = true
        return btn
    }()
    
    var didSetupConstraints = false
    
    var donationRequest: DonationRequest? {
        didSet {
            bloodGroupLabel.text = donationRequest?.bloodgroup
            hostpitalNameLabel.text = donationRequest?.hospital.title
            
            if let hospital = donationRequest?.hospital {
                self.mapView.addAnnotation(hospital)
                let cl = CLLocation(latitude: hospital.coordinate.latitude, longitude: hospital.coordinate.longitude)
                self.centerMapOnLocation(location: cl)
//                self.mapView.setCenter(hospital.coordinate, animated: true)
            }
            
        }
    }
    
    
    override func loadView() {
        
        view = UIView()
        view.addSubview(bloodGroupLabel)
        view.addSubview(hostpitalNameLabel)
        view.addSubview(mapView)
        view.addSubview(accpetbutton)
        view.backgroundColor = .white
        view.setNeedsUpdateConstraints() // bootstrap Auto Layout
        
    }
    
    override func updateViewConstraints() {
        if (!didSetupConstraints) {
            
            bloodGroupLabel.autoSetDimensions(to: CGSize(width: 100.0, height: 100.0))
            bloodGroupLabel.autoPin(toTopLayoutGuideOf: self, withInset: 10.0)
            bloodGroupLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 10.0)
            
            hostpitalNameLabel.autoPinEdge(.left, to: .right, of: bloodGroupLabel, withOffset: 10)
            hostpitalNameLabel.autoAlignAxis(.horizontal, toSameAxisOf: bloodGroupLabel)
            hostpitalNameLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 10.0)
            
            accpetbutton.autoPin(toBottomLayoutGuideOf: self, withInset: 10.0)
            accpetbutton.autoPinEdge(toSuperviewEdge: .left, withInset: 10.0)
            accpetbutton.autoPinEdge(toSuperviewEdge: .right, withInset: 10.0)
            accpetbutton.autoSetDimension(.height, toSize: 50.0)
            
            mapView.autoPinEdge(.top, to: .bottom, of: bloodGroupLabel, withOffset: 10)
            mapView.autoPinEdge(.bottom, to: .top, of: accpetbutton, withOffset: -10)
            mapView.autoPinEdge(toSuperviewEdge: .left, withInset: 0)
            mapView.autoPinEdge(toSuperviewEdge: .right, withInset: 0)
            
            didSetupConstraints = true
        }
        
        
        super.updateViewConstraints()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        SVProgressHUD.setDefaultMaskType(.black)
        
        
        mapView.delegate = self
//        let initialLocation = CLLocation(latitude: 3.142330, longitude: 101.671980)
//        centerMapOnLocation(location: initialLocation)
        
        accpetbutton.addTarget(self, action: #selector(acceptRequest), for: .touchUpInside)
        
        
    }
    
    class JsonError: JSONDecodable {
        var msg: String?
        required init(json: JSON) throws {
            print(json)
            msg = json["message"].stringValue
            
        }
    }
    
    class JsonSuccess: JSONDecodable {
        required init(json: JSON) throws {
            
        }
    }
    
    let tron = TRON(baseURL: Constants.baseUrl)
    
    func acceptRequest() {
        SVProgressHUD.show()
        if let authkey = UserDefaults.standard.string(forKey: Constants.authKey) {
            if authkey != "" {
                let request: APIRequest<JsonSuccess, JsonError> = tron.request("/request/accept")
                request.method = .post
                request.headerBuilder = HeaderBuilder(defaultHeaders: ["Authorization": authkey, "Content-type application":"json", "Accept application" : "json"])
                request.parameters = ["blood_request_id":(donationRequest?.id)! as String]
                request.perform(withSuccess: { (success) in
                    SVProgressHUD.dismiss()
                    self.showAlert(title: "Success", description: "Please wait for the approval for the request from the hospital")
                }) { (error) in
                    print(error)
                    SVProgressHUD.dismiss()
                    self.showAlert(title: "Warning", description: (error.errorModel?.msg)!)
                }
            }
        } else {
            SVProgressHUD.dismiss()
        }
        
        
    
    }
    
    
    func setUpNavigationBar() {
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 195/255, green: 78/255, blue: 78/255, alpha:1)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationItem.title = "Request"
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,regionRadius * 2.0, regionRadius * 2.0)        
        mapView.setRegion(coordinateRegion, animated: false)
        
    }
    
    func showAlert(title: String, description: String) {
        let alertController = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default)
        self.present(alertController, animated: true)
        alertController.addAction(OKAction)
    }
}


extension BloodRequestViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? Hospital {
            let identifier = "pin"
            var view: MKPinAnnotationView
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
                as? MKPinAnnotationView {
                dequeuedView.annotation = annotation
                view = dequeuedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                view.canShowCallout = true
                view.calloutOffset = CGPoint(x: -5, y: 5)
                view.rightCalloutAccessoryView = UIButton(type:.detailDisclosure) as UIView
            }
            return view
        } else {
            print("Map problem!!!!")
        }
        return nil
    }
}
