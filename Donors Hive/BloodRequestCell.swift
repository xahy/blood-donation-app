//
//  BloodRequestCell.swift
//  Donors Hive
//
//  Created by Ismail Zahee on 11/7/16.
//  Copyright © 2016 Ismail Zahee. All rights reserved.
//

import UIKit

class BloodRequestCell: UITableViewCell {

    @IBOutlet weak var bloodGroupBackgroundView: UIView!
    @IBOutlet weak var bloodgrouptxt: UILabel!
    @IBOutlet weak var hospitaltxt: UILabel!
    @IBOutlet weak var titletxt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        titletxt.textColor = UIColor(red:0.78, green:0.38, blue:0.38, alpha:1.0)
        hospitaltxt.textColor = UIColor(red:0.22, green:0.68, blue:0.44, alpha:1.0)
        bloodGroupBackgroundView.layer.cornerRadius = bloodGroupBackgroundView.frame.height/2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)

       
    }
    
    func configureCell(donationRequest: DonationRequest) {
        self.bloodgrouptxt.text = donationRequest.bloodgroup
        self.titletxt.text = donationRequest.description
        self.hospitaltxt.text = donationRequest.hospital.title
    }
    
}
