//
//  Profile.swift
//  Donors Hive
//
//  Created by Ismail Xahee on 3/19/17.
//  Copyright © 2017 Ismail Zahee. All rights reserved.
//

import Foundation
import TRON
import SwiftyJSON


struct Profile {
    let donor:Donor
    let history: [BloodRequestDonor]?
    let daysLastDonated: String
    
    init(json: JSON) {
        self.donor = Donor(json: json["donor"])
        self.history = [BloodRequestDonor]()
        for ar in json["history"].array! {
            let bloodRequestDonor = BloodRequestDonor(json: ar)
            self.history?.append(bloodRequestDonor)
        }
        self.daysLastDonated = json["dayslastdonate"].stringValue
        
    }
    
}
